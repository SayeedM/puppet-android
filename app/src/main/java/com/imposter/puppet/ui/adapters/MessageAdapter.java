package com.imposter.puppet.ui.adapters;


import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.imposter.puppet.R;
import com.imposter.puppet.models.ChatMessage;
import com.imposter.puppet.models.Message;
import com.imposter.puppet.models.UserStatusMessage;

/**
 * List adapter for showing messages
 *
 * Created by sayeedm on 3/10/17.
 */

public class MessageAdapter extends BaseAdapter {

    private Context context;
    private List<Message> messagesItems;

    public MessageAdapter(Context context, List<Message> items) {
        this.context = context;
        this.messagesItems = items;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        Message m = messagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (m instanceof ChatMessage) {
            ChatMessage cm = (ChatMessage)m;
            // Identifying the message owner
            if (cm.isSelf()) {
                // message belongs to you, so load the right aligned layout
                convertView = mInflater.inflate(R.layout.list_item_message_right,
                        null);
            } else {
                // message belongs to other person, load the left aligned layout
                convertView = mInflater.inflate(R.layout.list_item_message_left,
                        null);
            }

            TextView lblFrom = (TextView) convertView.findViewById(R.id.lblMsgFrom);
            TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);

            txtMsg.setText(cm.getMessage());
            lblFrom.setText(cm.getUser());
        }else if (m instanceof UserStatusMessage){
            UserStatusMessage sm = (UserStatusMessage)m;
            convertView = mInflater.inflate(R.layout.list_item_user_status,
                    null);

            TextView status = (TextView)convertView.findViewById(R.id.status);

            String msg = sm.getUser() + " ";
            switch (sm.getStatus()){
                case ENTRY:
                    msg += " has joined the room";
                    break;
                case LEAVE:
                    msg += " has left the room";
                    break;
            }
            status.setText(msg);
        }


        return convertView;
    }

    public void addItem(Message message){
        messagesItems.add(message);
        notifyDataSetChanged();
    }
}
