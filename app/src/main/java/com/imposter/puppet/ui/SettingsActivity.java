package com.imposter.puppet.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.imposter.puppet.R;
import com.imposter.puppet.configs.WebSocketConfig;

/**
 * setting activity
 *
 * Created by sayeedm on 3/11/17.
 */
public class SettingsActivity extends AppCompatActivity {

    private EditText txtIp;
    private Button btnSave;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        txtIp = (EditText) findViewById(R.id.ip);
        txtIp.setText(WebSocketConfig.getInstance().getServerIp());

        btnSave = (Button)findViewById(R.id.save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebSocketConfig.getInstance().setServerIp(txtIp.getText().toString());
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
