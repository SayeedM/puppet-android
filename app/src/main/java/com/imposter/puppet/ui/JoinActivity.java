package com.imposter.puppet.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.imposter.puppet.R;
import com.imposter.puppet.configs.WebSocketConfig;

/**
 * User joins a session via this screen
 *
 * Created by sayeedm on 3/10/17.
 */
public class JoinActivity extends AppCompatActivity {

    private EditText txtName;

    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_join);

        WebSocketConfig.getInstance().load(this);

        txtName = (EditText)findViewById(R.id.name);
        Button btnJoin = (Button)findViewById(R.id.btn_join);
        ImageButton btnSettings = (ImageButton)findViewById(R.id.btn_settings);

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSettings();
            }
        });

        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = txtName.getText().toString().trim();
                if (!name.equals("")){
                    goToChatActivity(name);
                }else{
                    Toast.makeText(JoinActivity.this,
                            getString(R.string.name_not_empty), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void goToChatActivity(String name){
        Intent ii = new Intent(this, MainActivity.class);
        ii.putExtra(MainActivity.KEY_NAME, name);
        startActivity(ii);
        finish();
    }

    private void showSettings(){
        Intent ii = new Intent(this, SettingsActivity.class);
        startActivity(ii);
    }

}
