package com.imposter.puppet.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.imposter.puppet.R;
import com.imposter.puppet.configs.WebSocketConfig;
import com.imposter.puppet.models.ChatMessage;
import com.imposter.puppet.models.Message;
import com.imposter.puppet.models.UserStatusMessage;
import com.imposter.puppet.ui.adapters.MessageAdapter;
import com.imposter.puppet.websocket.PuppetClientCallback;
import com.imposter.puppet.websocket.PuppetClient;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements PuppetClientCallback{

    public static final String KEY_NAME = "name";

    private Button btnSend;
    private EditText txtInputMsg;

    // Chat messages list adapter
    private MessageAdapter adapter;

    // Client name
    private String name = null;

    private PuppetClient client;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Getting the person name from previous screen
        Intent i = getIntent();
        name = i.getStringExtra("name");

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setEnabled(false);

        txtInputMsg = (EditText) findViewById(R.id.inputMsg);
        ListView listViewMessages = (ListView) findViewById(R.id.list_view_messages);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sending message to web socket server
                String msg = txtInputMsg.getText().toString();
                if (!msg.trim().equals("")) {
                    client.send(msg);
                    txtInputMsg.setText("");

                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setUser(name);
                    chatMessage.setMessage(msg);
                    chatMessage.setSelf(true);
                    adapter.addItem(chatMessage);
                }
            }
        });

        adapter = new MessageAdapter(this, new ArrayList<Message>());
        listViewMessages.setAdapter(adapter);


    }

    @Override
    public void onResume(){
        super.onResume();
        client = new PuppetClient(name);
        client.setCallback(this);
        client.connect();
    }

    @Override
    public void onPause(){
        super.onPause();
        if (client != null)
            client.disconnect();
    }






    @Override
    public void onConnected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSend.setEnabled(true);
            }
        });
    }

    @Override
    public void onDisconnected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSend.setEnabled(false);
            }
        });
    }

    @Override
    public void onChatMessage(final ChatMessage message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.addItem(message);
            }
        });
    }

    @Override
    public void onStatusMessage(final UserStatusMessage message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.addItem(message);
            }
        });
    }

    @Override
    public void onError(Throwable t) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSend.setEnabled(false);
            }
        });
    }
}
