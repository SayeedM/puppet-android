package com.imposter.puppet.websocket;

import com.imposter.puppet.models.ChatMessage;
import com.imposter.puppet.models.Message;
import com.imposter.puppet.models.UserStatusMessage;

/**
 * Created by sayeedm on 3/10/17.
 */

public interface PuppetClientCallback {
    void onConnected();
    void onDisconnected();
    void onChatMessage(ChatMessage message);
    void onStatusMessage(UserStatusMessage message);
    void onError(Throwable t);
}
