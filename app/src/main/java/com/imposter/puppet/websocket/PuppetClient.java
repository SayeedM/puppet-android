package com.imposter.puppet.websocket;

import com.google.gson.Gson;
import com.imposter.puppet.configs.WebSocketConfig;
import com.imposter.puppet.models.ChatMessage;
import com.imposter.puppet.models.UserStatusMessage;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * The web socket client wrapper
 *
 * Created by sayeedm on 3/10/17.
 */
public class PuppetClient extends WebSocketListener {

    private static final int NORMAL_CLOSURE_STATUS = 1000;

    private OkHttpClient client;
    private WebSocket webSocket;

    private Gson gson;
    private String userName;

    private PuppetClientCallback callback;

    public PuppetClient(String userName){
        client = new OkHttpClient();
        gson = new Gson();

        this.userName = userName;
    }

    public void setCallback(PuppetClientCallback callback) {
        this.callback = callback;
    }

    public void connect(){
        String url = WebSocketConfig.getInstance().getWebSocketEndPoint();
        Request request = new Request.Builder().url(url).build();
        webSocket = client.newWebSocket(request, this);
    }

    public void disconnect(){
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        client.dispatcher().executorService().shutdown();
    }

    public void send(String message){
        ChatMessage msg = new ChatMessage();
        msg.setMessage(message);
        msg.setUser(userName);
        webSocket.send(gson.toJson(msg));
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        System.out.println("onOpen");
        UserStatusMessage msg = new UserStatusMessage();
        msg.setUser(userName);
        msg.setStatus(UserStatusMessage.Status.ENTRY);
        webSocket.send(gson.toJson(msg));

        if (callback != null) {
            callback.onConnected();
            msg.setSelf(true);
            callback.onStatusMessage(msg);
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        System.out.println("Receiving: " + text);



        if (callback != null){
            if (text.contains("message")) {
                ChatMessage cm = gson.fromJson(text, ChatMessage.class);
                callback.onChatMessage(cm);
            } else if (text.contains("status")){
                UserStatusMessage sm = gson.fromJson(text, UserStatusMessage.class);
                callback.onStatusMessage(sm);
            }
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("Receiving: " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        UserStatusMessage msg = new UserStatusMessage();
        msg.setUser(userName);
        msg.setStatus(UserStatusMessage.Status.LEAVE);
        webSocket.send(gson.toJson(msg));
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        System.out.println("Closing: " + code + " " + reason);

        if (callback != null)
            callback.onDisconnected();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }


}
