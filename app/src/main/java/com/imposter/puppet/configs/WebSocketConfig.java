package com.imposter.puppet.configs;


import android.content.Context;
import android.content.SharedPreferences;

/**
 *
 * Created by sayeedm on 3/10/17.
 */
public class WebSocketConfig {
    private static WebSocketConfig instance;
    private WebSocketConfig(){

    }

    public static WebSocketConfig getInstance(){
        if (instance == null)
            instance = new WebSocketConfig();
        return instance;
    }

    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    private final String PREF_NAME = "puppet_prefs";
    private final String KEY_SERVER_IP = "server_ip";
    private final String DEFAULT_SERVER_IP = "10.10.10.10:8080";

    public void load(Context context){
        sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public String getWebSocketEndPoint(){
        String server = sp.getString(KEY_SERVER_IP, DEFAULT_SERVER_IP);
        if (server.endsWith("/"))
            server = server.substring(0, server.length() - 2);
        return "ws://" + server + "/name";
    }

    public String getServerIp(){
        return sp.getString(KEY_SERVER_IP, DEFAULT_SERVER_IP);
    }

    public void setServerIp(String ip){
        spEditor.putString(KEY_SERVER_IP, ip);
        spEditor.commit();
    }


}
