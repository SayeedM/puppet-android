package com.imposter.puppet.models;

/**
 * Represents a chat message
 *
 * Created by sayeedm on 3/10/17.
 */
public class ChatMessage extends Message {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
