package com.imposter.puppet.models;

/**
 * when a user enters or leaves
 * Created by sayeedm on 3/10/17.
 */
public class UserStatusMessage extends Message {

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status{
        ENTRY, LEAVE
    }

    private Status status;

    public Status getStatus() {
        return status;
    }


}
