package com.imposter.puppet.models;

/**
 * Created by sayeedm on 3/10/17.
 */

public class Message {
    private String user;
    private Long timestamp;
    private boolean isSelf = false;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }
}
